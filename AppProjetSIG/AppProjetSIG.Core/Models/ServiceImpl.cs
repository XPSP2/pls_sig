﻿namespace AppProjetSIG.Core.Models
{
    using AppProjetSIG.Core.SQL.Interface;
    using AppProjetSIG.Core.SQL.Dao;
    using AppProjetSIG.Core.SQL.Table;
    using System;
    using System.Collections.Generic;
    using System.Net.Sockets;
    using System.Threading.Tasks;

    /// <summary>
    /// Defines the <see cref="ServiceImpl" />
    /// </summary>
    public class ServiceImpl : IService
    {
        /// <summary>
        /// Boolean to know if the app is used online or offline
        /// </summary>
        private bool Connected { get; set; } = true;

        private static ITable DataBase = new Table();

        public ServiceImpl()
        {
            try
            {
                throw new Exception();
                DataBase.Connexion();
                Connected = true;
            } catch (Exception)
            {
                //TODO : rajouter le code pour se connecter sur la db local
                Connected = false;
                DataBase = new TableSQLite();
                DataBase.Connexion();
            }
        }

        ~ServiceImpl()
        {
            DataBase.Deconnexion();
        }

        public async Task<CentreInteret> AddStructure(CentreInteret ci)
        {
            DataBase.Insert(ci);

            return await Task.FromResult(ci);
        }
        
        public double ComputeDistance(double usersLon, double usersLat, int locationId)
        {
            var location = DataBase.Find(locationId);
            return Math.Sqrt(
                    Math.Pow(usersLon - location.Longitude, 2) +
                    Math.Pow(usersLat - location.Latitude, 2)
                );
        }
        
        public List<string> GetAvailableNatures()
        {
            return NatureList.GetNatures();
        }
        
        public async Task<string> GetDetails(int locationId)
        {
            var location = DataBase.Find(locationId);
            return await Task.FromResult(location.Detail);
        }
        
        public async Task<Nature> GetNature(int locationId)
        {
            var location = DataBase.Find(locationId);
            return await Task.FromResult(location.Nature);
        }

        public async Task<string> GetNom(int locationId)
        {
            var location = DataBase.Find(locationId);
            return await Task.FromResult(location.Nom);
        }

        public void SwitchConnection()
        {
            if(Connected)
            {
                Synchronise(new TableSQLite());
                DataBase.Deconnexion();
                DataBase = new TableSQLite();
                DataBase.Connexion();
            }
            else
            {
                Synchronise(new Table());
                DataBase = new Table();
                DataBase.Connexion();
            }
            Connected = !Connected;
        }

        public void Synchronise(ITable toSynchronise)
        {
            toSynchronise.Connexion();
            List<CentreInteret> centreInterets = DataBase.FindAll();
            if(!ContainsAll(toSynchronise.FindAll(), centreInterets))
            {
                foreach(CentreInteret ci in centreInterets)
                {
                    if (toSynchronise.Find(ci.Id) == null)
                        toSynchronise.Insert(ci);
                }
            }
            toSynchronise.Deconnexion();
        }

        private bool ContainsAll(List<CentreInteret> first, List<CentreInteret> second)
        {
            if(first.Count != second.Count)
            {
                return false;
            }
            foreach(CentreInteret ci in second)
            {
                if (!first.Contains(ci))
                    return false;
            }
            return true;
        }

        public bool GetConnected()
        {
            return Connected;
        }

        public async Task<CentreInteret> FindCentreInteretById(int id)
        {
            return await Task.FromResult(DataBase.Find(id));
        }

        public async Task<List<CentreInteret>> FindAllCentreInteret()
        {
            return await Task.FromResult(DataBase.FindAll());
        }

        public async Task<bool> UpdateStructure(CentreInteret ci)
        {
            return await Task.FromResult(DataBase.Update(ci));
        }

        public async Task<CentreInteret> RemoveStructure(int Id)
        {
            return await Task.FromResult(DataBase.Remove(Id));
        }
    }
}
