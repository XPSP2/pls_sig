﻿namespace AppProjetSIG.Core.Models
{
    using AppProjetSIG.Core.SQL.Interface;
    using AppProjetSIG.Core.SQL.Table;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    /// <summary>
    /// Defines the <see cref="IService" />
    /// </summary>
    public interface IService
    {
        /// <summary>
        /// The GetNature
        /// </summary>
        /// <param name="locationId">The location's Id, to retrieve the structure form DB.</param>
        /// <returns>The nature of given structure.</returns>
        Task<Nature> GetNature(int locationId);

        /// <summary>
        /// The GetDetails
        /// </summary>
        /// <param name="locationId">The location's Id, to retrieve the structure form DB.</param>
        /// <returns>The name of given structure.</returns>
        Task<string> GetNom(int locationId);

        /// <summary>
        /// The GetDetails
        /// </summary>
        /// <param name="locationId">The location's Id, to retrieve the structure form DB.</param>
        /// <returns>The details of given structure.</returns>
        Task<string> GetDetails(int locationId);

        /// <summary>
        /// This method access the DB and get all natures, if the device's offline then the method return the following static list :<br/>
        /// {Hopitaux; Cabinets Medicaux; Salles de Sport; Parcours de santé}
        /// </summary>
        /// <returns>A list of structure's nature</returns>
        List<string> GetAvailableNatures();

        /// <summary>
        /// This method gets the device's location then the user define the structure's nature and give details.
        /// If the user uses the app offline, the structure will be stored locally and push onto the DB when connected.<br/><br/>
        /// 
        /// Maybe the user will be able to create structure to a location he is not?
        /// </summary>
        /// <param name="longitude">Give the user's longitude, accessed via the device's GPS</param>
        /// <param name="latitude">Give the user's latitude, accessed via the device's GPS</param>
        /// <param name="nature">The nature parameter is given by the list of available natures in the DB.</param>
        /// <param name="details">The details parameter is given by the user.</param>
        /// <returns>A newly created Structure that will be added to the DB when connected</returns>
        Task<CentreInteret> AddStructure(CentreInteret ci);

        /// <summary>
        /// Computes the euclidian distance between the user's current location and the desired structure's location.
        /// </summary>
        /// <param name="usersLon">Give the user's longitude, accessed via the device's GPS</param>
        /// <param name="usersLat">Give the user's latitude, accessed via the device's GPS</param>
        /// <param name="locationsLon">Location's longitude, given by the DB.</param>
        /// <param name="locationsLat">Location's latitude, given by the DB.</param>
        /// <returns>The distance between two points.</returns>
        double ComputeDistance(double usersLon, double usersLat, int locationId);

        /// <summary>
        /// Change the connection mode of the application.
        /// </summary>
        void SwitchConnection();

        bool GetConnected();

        void Synchronise(ITable toSynchronise);

        Task<CentreInteret> FindCentreInteretById(int id);

        Task<List<CentreInteret>> FindAllCentreInteret();

        Task<bool> UpdateStructure(CentreInteret ci);

        Task<CentreInteret> RemoveStructure(int Id);
    }
}
