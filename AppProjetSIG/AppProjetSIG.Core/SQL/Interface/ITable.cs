﻿using AppProjetSIG.Core.SQL.Table;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AppProjetSIG.Core.SQL.Interface
{
    public interface ITable
    {
        Task Connexion();

        bool Insert(CentreInteret CI);

        CentreInteret Find(int Id);

        List<CentreInteret> FindAll();

        bool Update(CentreInteret CI);

        CentreInteret Remove(int Id);

        void Deconnexion();
    }
}
