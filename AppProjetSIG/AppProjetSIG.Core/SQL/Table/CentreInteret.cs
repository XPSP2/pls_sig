﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Data;

namespace AppProjetSIG.Core.SQL.Table
{
    [Table("centre_interet")]
    public class CentreInteret
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public string Nom{ get; set; }
        public Nature Nature { get; set; }
        public string Detail { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }

        public override string ToString()
        {
            return Nom;
        }

    }
}