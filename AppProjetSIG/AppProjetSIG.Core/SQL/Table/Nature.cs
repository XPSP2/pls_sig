﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace AppProjetSIG.Core.SQL.Table
{
    public enum Nature
    {
        [EnumString("Hôpital")]
        HOPITAL,

        [EnumString("Cabinet médical")]
        CABINETMEDICAUX,

        [EnumString("Salle de sport")]
        SALLESPORTS,

        [EnumString("Parcours de santé")]
        SANTE,

        [EnumString("Autre")]
        AUTRE
    }

    public class NatureList
    {
        public static List<string> GetNatures()
        {
            List<string> list = new List<string>();

            foreach (Nature nature in Enum.GetValues(typeof(Nature)))
            {
                list.Add(StringEnum.GetStringValue(nature));
            }

            return list;
        }

        public static Nature GetValue(string value) {
            List<string> list = GetNatures();
            int i = list.FindIndex(s => s.Equals(value));

            return (Nature) i;
        }
    }

    public class EnumStringAttribute : Attribute
    {
        public string StringValue { get; set; }

        public EnumStringAttribute(string stringValue)
        {
            this.StringValue = stringValue;
        }
    }

    public static class StringEnum
    {
        public static string GetStringValue(Enum value)
        {
            string output = null;

            Type type = value.GetType();
            FieldInfo fi = type.GetField(value.ToString());
            EnumStringAttribute[] attrs = fi.GetCustomAttributes(typeof(EnumStringAttribute), false) as EnumStringAttribute[];

            if (attrs.Length > 0)
            {
                output = attrs[0].StringValue;
            }

            return output;
        }
    }
}
