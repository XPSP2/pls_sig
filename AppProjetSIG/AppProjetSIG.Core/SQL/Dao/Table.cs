﻿using AppProjetSIG.Core.SQL.Interface;
using AppProjetSIG.Core.SQL.Table;
using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace AppProjetSIG.Core.SQL.Dao
{
    public class Table : ITable
    {
        //private string Addr = "Server=sig;Port=5432;Database=centreinteret;User Id=postgres;Password=admin;";
       // private string Addr = "Host=127.0.0.1; Port=5432;Username=postgres;Password=admin;Database=sig.centreinteret";
        //private string Addr = "server=localhost; port=5432; user=postgres; pwd=admin;database=centreinteret";
        private string Addr = "Server=localhost;Port=5432;Database=centreinteret;User Id=postgres;Password=admin";
        NpgsqlCommand npgsqlCommand = null;
        NpgsqlConnection npgsqlConnection= null;
   


        public async Task Connexion()
        {
            npgsqlConnection = new NpgsqlConnection(Addr);
            await npgsqlConnection.OpenAsync();
            await npgsqlConnection.WaitAsync();
        
        }

        public void Deconnexion()
        {
            npgsqlConnection.Close();
        }

         public CentreInteret Find(int Id)
       
        {
            DataTable Data = new DataTable();
            NpgsqlDataAdapter NpgsqlDA;
            string select = "SELECT FROM centre_interet WHERE(id="  + Id + ")";
            npgsqlCommand = new NpgsqlCommand(select, npgsqlConnection);
            NpgsqlDA = new NpgsqlDataAdapter(npgsqlCommand);
            NpgsqlDA.Fill(Data);
            //Convertir DataTable(qui ne contient qu'un enregistrement) en objet CentreInteret
            CentreInteret lstData = FromDataTable(Data);
            return lstData;
        }


        public List<CentreInteret> FindAll()
        {
            DataTable Data = new DataTable();
            NpgsqlDataAdapter NpgsqlDA;

            string select = "SELECT * FROM centre_interet";
            npgsqlCommand = new NpgsqlCommand(select, npgsqlConnection);
            NpgsqlDA = new NpgsqlDataAdapter(npgsqlCommand);
            NpgsqlDA.Fill(Data);
            //Convertir DataTable en Liste de CentreInteret
            List<CentreInteret> lstData = BindList<CentreInteret>(Data);
            return lstData;

        }

        public bool Insert(CentreInteret CI)
        {
            npgsqlConnection = new NpgsqlConnection(Addr);
            string insert = "INSERT INTO centre_interet values (DEFAULT, " +
                (int)CI.Nature + ", " +
                CI.Detail + "," +
                CI.Latitude + "," +
                CI.Longitude + " )";


            npgsqlCommand = new NpgsqlCommand(insert, npgsqlConnection);

            /*npgsqlCommand.Parameters.Add(new NpgsqlParameter("nom", NpgsqlDbType.Varchar)).Value = CI.Nom;
            npgsqlCommand.Parameters.Add(new NpgsqlParameter("nature", NpgsqlDbType.Integer)).Value = CI.Nature;
            npgsqlCommand.Parameters.Add(new NpgsqlParameter("detail", NpgsqlDbType.Text)).Value = CI.Detail;
            npgsqlCommand.Parameters.Add(new NpgsqlParameter("latitude", NpgsqlDbType.Double)).Value = CI.Latitude;
            npgsqlCommand.Parameters.Add(new NpgsqlParameter("longitude", NpgsqlDbType.Double)).Value = CI.Longitude;*/
            
            npgsqlCommand.ExecuteNonQuery(); //Exécution
            return true;


        }

        public CentreInteret Remove(int Id)
        {
            /*string delete = "DELETE FROM \"centreinteret\" WHERE(Id=:pid)";
            npgsqlCommand = new NpgsqlCommand(delete, npgsqlConnection);
            npgsqlCommand.Parameters.Add(new NpgsqlParameter("pid", NpgsqlDbType.Integer)).Value = Id;
            npgsqlCommand.ExecuteNonQuery();
            Deconnexion();*/
            DataTable Data = new DataTable();
            NpgsqlDataAdapter NpgsqlDA;
            string delete = "DELETE FROM centre_interet WHERE(id=" + Id + ")";
            var cI = Find(Id);
            npgsqlCommand = new NpgsqlCommand(delete, npgsqlConnection);
            NpgsqlDA = new NpgsqlDataAdapter(npgsqlCommand);
            NpgsqlDA.Fill(Data);
            //Convertir DataTable(qui ne contient qu'un enregistr'ement) en objet CentreInteret
            CentreInteret myData = FromDataTable(Data);
            return cI;
        }

        public bool Update(CentreInteret CI)
        {

            string update = "UPDATE centre_interet SET " +
                CI.Nature + ", " +
                CI.Detail + "," +
                CI.Latitude + "," +
                CI.Longitude + " WHERE(id=" + CI.Id + ");";
            npgsqlCommand = new NpgsqlCommand(update, npgsqlConnection);

            //Définition et ajout des paramètres 
            /*npgsqlCommand.Parameters.Add(new NpgsqlParameter("Nom", NpgsqlDbType.Varchar)).Value = CI.Nom;
            npgsqlCommand.Parameters.Add(new NpgsqlParameter("Nature", NpgsqlDbType.Integer)).Value = CI.Nature;
            npgsqlCommand.Parameters.Add(new NpgsqlParameter("Detail", NpgsqlDbType.Varchar)).Value = CI.Detail;
            npgsqlCommand.Parameters.Add(new NpgsqlParameter("Latitude", NpgsqlDbType.Double)).Value = CI.Latitude;
            npgsqlCommand.Parameters.Add(new NpgsqlParameter("Longitude", NpgsqlDbType.Double)).Value = CI.Longitude;*/

            

            npgsqlCommand.ExecuteNonQuery();//Exécution
            return true;
        }

        internal static CentreInteret FromDataTable(DataTable data)
        {
            throw new NotImplementedException();
        }


        public static List<T> BindList<T>(DataTable dt)
        {
            // Example 1:
            // Get private fields + non properties
            //var fields = typeof(T).GetFields(BindingFlags.NonPublic | BindingFlags.Instance);

            // Example 2: Your case
            // Get all public fields
            var fields = typeof(T).GetFields();

            List<T> lst = new List<T>();

            foreach (DataRow dr in dt.Rows)
            {
                // Create the object of T
                var ob = Activator.CreateInstance<T>();

                foreach (var fieldInfo in fields)
                {
                    foreach (DataColumn dc in dt.Columns)
                    {
                        // Matching the columns with fields
                        if (fieldInfo.Name == dc.ColumnName)
                        {
                            // Get the value from the datatable cell
                            object value = dr[dc.ColumnName];

                            // Set the value into the object
                            fieldInfo.SetValue(ob, value);
                            break;
                        }
                    }
                }

                lst.Add(ob);
            }

            return lst;
        }

    }
}
