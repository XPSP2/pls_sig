﻿using AppProjetSIG.Core.SQL.Interface;
using AppProjetSIG.Core.SQL.Table;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using SQLite;
using System.IO;
using System.Data;

namespace AppProjetSIG.Core.SQL.Dao
{
    public class TableSQLite : ITable
    {

        private string Addr = Path.Combine(
            Environment.GetFolderPath(Environment.SpecialFolder.Personal),
            "centreinteret.db3"
            );
        SQLiteConnection sqliteConnection;

        public async Task Connexion()
        {
            sqliteConnection = new SQLiteConnection(Addr);
            sqliteConnection.CreateTable<CentreInteret>();
        }

        public void Deconnexion()
        {
            sqliteConnection.Close();
        }

        public CentreInteret Find(int Id)
        {
            return sqliteConnection.Find<CentreInteret>(Id);
        }

        public List<CentreInteret> FindAll()
        {
            var centreInteret = new List<CentreInteret>();
            var sqlQueryEnumerable = sqliteConnection.CreateCommand("select * from centre_interet").ExecuteDeferredQuery<CentreInteret>();
            foreach(var row in sqlQueryEnumerable)
            {
                centreInteret.Add(row);
            }
            return centreInteret;
        }

        public bool Insert(CentreInteret CI)
        {
            if(sqliteConnection.Find<CentreInteret>(CI.Id) == null)
            {
                sqliteConnection.Insert(CI);
                return true;
            }
            return false;
        }

        public CentreInteret Remove(int Id)
        {
            var ci = sqliteConnection.Find<CentreInteret>(Id);
            sqliteConnection.Delete<CentreInteret>(Id);
            return ci;
        }

        public bool Update(CentreInteret CI)
        {
            sqliteConnection.Update(CI);
            return true;
        }
    }
}
