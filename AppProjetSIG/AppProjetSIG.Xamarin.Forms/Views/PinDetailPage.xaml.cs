﻿using AppProjetSIG.Core.SQL.Table;
using AppProjetSIG.Xamarin.Forms.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Maps;
using Xamarin.Forms.Xaml;

namespace AppProjetSIG.Xamarin.Forms.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PinDetailPage : ContentPage
    {
        PinDetailViewModels PinDetailViewModels;


        public PinDetailPage(CentreInteret ci, PinDetailViewModels pinDetailViewModels)
        {
            InitializeComponent();

            LoadPin(ci);

            BindingContext = PinDetailViewModels = pinDetailViewModels;

            MessagingCenter.Subscribe<AddPage, string>(this, "UpdateCI", async (obj, str) =>
            {
                var centre_interet = await PinDetailViewModels.FindCentreInteretById(PinDetailViewModels.CentreInteret.Id);
                PinDetailViewModels.CentreInteret = centre_interet;

                PinDetailViewModels.Title = PinDetailViewModels.CentreInteret.Nom;
                PinDetailViewModels.Nature = StringEnum.GetStringValue(centre_interet.Nature);
                LoadPin(centre_interet);

                await PinDetailViewModels.ExecuteAsync();
            });
        }

        public PinDetailPage()
        {
            InitializeComponent();

            var ci = new CentreInteret
            {
                Nom = "Empty",
                Nature = Nature.AUTRE,
                Detail = "Lorem Ipsum",
                Latitude = 0.0f,
                Longitude = 0.0f
            };

            LoadPin(ci);

            PinDetailViewModels = new PinDetailViewModels(ci);
            BindingContext = PinDetailViewModels;
        }

        public void LoadPin(CentreInteret ci)
        {
            map.Pins.Clear();

            map.Pins.Add(new Pin
            {
                Label = ci.Nom,
                Position = new Position(ci.Latitude, ci.Longitude)
            });

            map.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(ci.Latitude, ci.Longitude), Distance.FromMiles(0.1)));
        }

        async void Update_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushModalAsync(new NavigationPage(new AddPage(PinDetailViewModels.CentreInteret)));
        }

        async void Remove_Clicked(object sender, EventArgs e)
        {
            await PinDetailViewModels.RemoveAsync();
            MessagingCenter.Send(this, "RemoveCI", "rem");
            await Navigation.PopAsync();
        }
    }
}