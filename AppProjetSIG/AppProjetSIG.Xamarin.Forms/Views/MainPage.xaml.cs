﻿using AppProjetSIG.Xamarin.Forms.ViewModels;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AppProjetSIG.Xamarin.Forms.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MainPage : MasterDetailPage
	{
        Dictionary<int, NavigationPage> MenuPages = new Dictionary<int, NavigationPage>();

		public MainPage ()
		{
			InitializeComponent();

            MasterBehavior = MasterBehavior.Popover;
            MenuPages.Add((int)Item.Menu.Carte, (NavigationPage)Detail);
		}

        public async Task NavigateMenu(int Id)
        {
            if (!MenuPages.ContainsKey(Id))
            {
                switch(Id)
                {
                    case (int)Item.Menu.Carte:
                        MenuPages.Add(Id, new NavigationPage(new MapPage()));
                        break;

                    case (int)Item.Menu.Parametres:
                        MenuPages.Add(Id, new NavigationPage(new SettingsPage()));
                        break;
                }
            }

            var newPage = MenuPages[Id];

            if (newPage != null && Detail != newPage)
            {
                Detail = newPage;

                if (Device.RuntimePlatform == Device.Android)
                    await Task.Delay(100);

                IsPresented = false;
            }
        }
    }
}