﻿using System.Collections.Generic;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using MenuItem = AppProjetSIG.Xamarin.Forms.Item.MenuItem;

namespace AppProjetSIG.Xamarin.Forms.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MenuPage : ContentPage
	{
        MainPage RootPage { get => Application.Current.MainPage as MainPage; }
        List<MenuItem> List;

		public MenuPage ()
		{
			InitializeComponent ();

            List = new List<MenuItem>
            {
                new MenuItem {Id = Item.Menu.Carte, Title = "Carte"},
                new MenuItem {Id = Item.Menu.Parametres, Title = "Paramètres"}
            };

            listView.ItemsSource = List;

            listView.SelectedItem = List[0];
            listView.ItemSelected += async (sender, e) =>
            {
                if (e.SelectedItem == null)
                    return;

                var id = (int)((MenuItem)e.SelectedItem).Id;
                await RootPage.NavigateMenu(id);
            };
        }
	}
}