﻿using AppProjetSIG.Xamarin.Forms.ViewModels;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AppProjetSIG.Xamarin.Forms.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SettingsPage : ContentPage
	{
        SettingsViewModels SettingsViewModels;


		public SettingsPage ()
		{
			InitializeComponent ();

            BindingContext = SettingsViewModels = new SettingsViewModels();
		}

        private void Switch_Toggled(object sender, ToggledEventArgs e)
        {
            var item = sender as Switch;
            item.IsEnabled = false;

            Task task = new Task(() =>
            {
                var b = SettingsViewModels.SwitchConnection();

                item.IsEnabled = true;
                item.IsToggled = b;
            });

            task.Start();
            task.Wait();
        }
    }
}