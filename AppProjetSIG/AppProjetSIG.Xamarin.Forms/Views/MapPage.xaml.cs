﻿using Xamarin.Forms;
using Xamarin.Forms.Maps;
using System.Collections.Generic;
using AppProjetSIG.Xamarin.Forms.ViewModels;
using AppProjetSIG.Core.SQL.Table;
using Xamarin.Forms.Xaml;
using System;
using System.Threading.Tasks;
using AppProjetSIG.Xamarin.Forms.Maps;
using Xamarin.Essentials;

namespace AppProjetSIG.Xamarin.Forms.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MapPage : ContentPage
	{
        MapViewModels MapViewModels;

        public Command LoadPinCommand { get; set; }

        private bool runOnly = true;

        public MapPage ()
		{
			InitializeComponent ();

            BindingContext = MapViewModels = new MapViewModels();

            LoadPinCommand = new Command(async () => await ExecuteLoadPinsCommand());
            LoadPinCommand.Execute(0);

            MessagingCenter.Subscribe<AddPage, string>(this, "AddCI", (obj, str) => LoadPinCommand.Execute(0));
            MessagingCenter.Subscribe<PinDetailPage, string>(this, "RemoveCI", (obj, str) => LoadPinCommand.Execute(0));
        }

        async Task ExecuteLoadPinsCommand()
        {
            map.Pins.Clear();

            if (runOnly)
            {
                var locator = await Geolocation.GetLastKnownLocationAsync();
                map.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(locator.Latitude, locator.Longitude), Distance.FromMiles(1.0)));

                runOnly = false;
            }

            var ciList = await MapViewModels.GetCentreInterets();

            if (ciList.Count == 0)
            {
                CreateDataBase(ciList);
            }

            foreach (var ci in ciList)
            {
                var pin = new CIPin
                {
                    CIId = ci.Id,
                    Type = PinType.Place,
                    Label = ci.Nom,
                    Nature = ci.Nature,
                    Detail = ci.Detail,
                    Position = new Position(ci.Latitude, ci.Longitude)
                };

                pin.Clicked += async (sender, args) => await OnItemSelected(sender, args);
                map.Pins.Add(pin);
            }
        }

        async Task OnItemSelected(object sender, EventArgs args)
        {
            var pin = sender as CIPin;
            if (pin == null)
               return;

            var ct = pin.ToCentreInteret();

            await Navigation.PushAsync(new PinDetailPage(ct, new PinDetailViewModels(ct)));

            if (Device.RuntimePlatform == Device.UWP)
            {
                LoadPinCommand.Execute(0);
            }
        }

        async void AddItem_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushModalAsync(new NavigationPage(new AddPage()));

            if (Device.RuntimePlatform == Device.UWP)
            {
                LoadPinCommand.Execute(0);
            }
        }

        public void CreateDataBase(List<CentreInteret> ciList)
        {
            var ci = new CentreInteret
            {
                Id = 1,
                Nom = "Le Commencement",
                Nature = Nature.SALLESPORTS,
                Detail = "C'est ici que tout à commencé",
                Latitude = 47.845273,
                Longitude = 1.939768
            };
            Main.Service.AddStructure(ci);

            ciList.Add(ci);
        }
    }
}