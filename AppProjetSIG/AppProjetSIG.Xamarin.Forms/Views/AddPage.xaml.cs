﻿using AppProjetSIG.Core.SQL.Table;
using AppProjetSIG.Xamarin.Forms.Maps;
using AppProjetSIG.Xamarin.Forms.ViewModels;
using System;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Maps;
using Xamarin.Forms.Xaml;

namespace AppProjetSIG.Xamarin.Forms.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AddPage : ContentPage
	{
        AddViewModels AddViewModels;
        public Command LoadCommand { get; set; }

        public bool update = false;

        public AddPage(CentreInteret ci)
        {
            InitializeComponent();

            update = true;

            BindingContext = AddViewModels = new AddViewModels(ci);

            LoadCommand = new Command(async () => await ExecuteCommand());
            LoadCommand.Execute(0);
        }

        public AddPage ()
		{
			InitializeComponent ();
            
            BindingContext = AddViewModels = new AddViewModels();

            LoadCommand = new Command(async () => await ExecuteCommand());
            LoadCommand.Execute(0);
        }

        private void Button_Clicked(object sender, EventArgs e)
        {
            map.Pins[0].Position = map.VisibleRegion.Center;
        }

        async Task ExecuteCommand()
        {
            if (!update)
            {
                var locator = await Geolocation.GetLastKnownLocationAsync();
                AddViewModels.CIPin.Position = new Position(locator.Latitude, locator.Longitude);
            }

            map.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(AddViewModels.CIPin.Position.Latitude, AddViewModels.CIPin.Position.Longitude), Distance.FromMiles(0.5)));
            map.Pins.Add(AddViewModels.CIPin);
        }

        async void Save_Clicked(object sender, EventArgs e)
        {
            AddViewModels.CIPin.Position = map.Pins[0].Position;

            if (!update)
            {
                AddViewModels.AddStructure();
            }
            else
            {
                AddViewModels.UpdateStructure();
                MessagingCenter.Send(this, "UpdateCI", "upd");
            }

            MessagingCenter.Send(this, "AddCI", "add");
            await Navigation.PopModalAsync();
        }

        async void Cancel_Clicked(object sender, EventArgs e)
        {
            await Navigation.PopModalAsync();
        }
    }
}