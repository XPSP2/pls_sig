﻿using AppProjetSIG.Core.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace AppProjetSIG.Xamarin.Forms
{
    public class Main : Application
    {
        public static IService Service;

        public static double ScreenHeight;
        public static double ScreenWidth;

        public Main()
        {
            MainPage = new Views.MainPage();
        }
    }
}
