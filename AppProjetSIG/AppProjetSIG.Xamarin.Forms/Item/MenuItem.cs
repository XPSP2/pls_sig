﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AppProjetSIG.Xamarin.Forms.Item
{
    public enum Menu
    {
        Carte,
        Parametres
    }

    public class MenuItem
    {
        public Menu Id { get; set; }
        public string Title { get; set; }
    }
}
