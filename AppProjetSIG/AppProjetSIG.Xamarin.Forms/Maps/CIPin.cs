﻿using AppProjetSIG.Core.SQL.Table;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms.Maps;

namespace AppProjetSIG.Xamarin.Forms.Maps
{
    public class CIPin : Pin
    {
        public int CIId { get; set; }
        public Nature Nature { get; set; }
        public string Detail { get; set; }


        public void ToPin(CentreInteret ci)
        {
            Id = ci.Id;
            Type = PinType.Place;
            Label = ci.Nom;
            Nature = ci.Nature;
            Detail = ci.Detail;
            Position = new Position(ci.Latitude, ci.Longitude);
        }

        public CentreInteret ToCentreInteret()
        {
            CentreInteret ci = new CentreInteret
            {
                Id = CIId,
                Nom = Label,
                Nature = Nature,
                Detail = Detail,
                Latitude = Position.Latitude,
                Longitude = Position.Longitude
            };

            return ci;
        }
    }
}
