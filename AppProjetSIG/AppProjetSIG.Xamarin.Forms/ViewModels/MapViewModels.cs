﻿using AppProjetSIG.Core.SQL.Table;
using AppProjetSIG.Xamarin.Forms.Maps;
using AppProjetSIG.Xamarin.Forms.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace AppProjetSIG.Xamarin.Forms.ViewModels
{
    public class MapViewModels : BaseViewModels
    {
        public MapViewModels()
        {
            Title = "Carte";
        }

        public async Task<List<CentreInteret>> GetCentreInterets()
        {
            return await Main.Service.FindAllCentreInteret();
        }
    }
}
