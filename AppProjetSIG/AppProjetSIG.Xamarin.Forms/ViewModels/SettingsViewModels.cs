﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AppProjetSIG.Xamarin.Forms.ViewModels
{
    public class SettingsViewModels : BaseViewModels
    {
        public bool Connected { get; set; }

        public SettingsViewModels()
        {
            Title = "Paramètres";
            Connected = Main.Service.GetConnected();
        }

        public bool SwitchConnection()
        {
            try
            {
                Main.Service.SwitchConnection();
                return true;
            } catch
            {
                return false;
            }
        }
    }
}
