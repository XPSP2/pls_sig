﻿using System;
using System.Collections.Generic;
using System.Text;
using AppProjetSIG.Core.SQL.Table;
using AppProjetSIG.Xamarin.Forms.Maps;
using Xamarin.Forms.Maps;

namespace AppProjetSIG.Xamarin.Forms.ViewModels
{
    public class AddViewModels : BaseViewModels
    {
        public IList<string> ItemSource { get; set; }

        public CIPin CIPin { get; set; }
        public string Nature { get; set; }

        public AddViewModels(CentreInteret ci = null)
        {
            Title = ci == null ? "Ajouter un point d'intérêt" : "Modifier un point d'intérêt";
            ItemSource = Main.Service.GetAvailableNatures();

            CIPin = new CIPin {
                Label = ""
            };

            if (ci != null)
            {
                CIPin.CIId = ci.Id;
                CIPin.Label = ci.Nom;
                CIPin.Nature = ci.Nature;
                CIPin.Detail = ci.Detail;
                CIPin.Position = new Position(ci.Latitude, ci.Longitude);

                Nature = Main.Service.GetAvailableNatures().Find(e => e.Equals(StringEnum.GetStringValue(CIPin.Nature)));
            }
        }

        public async void AddStructure()
        {
            CIPin.Nature = NatureList.GetValue(Nature);

            await Main.Service.AddStructure(CIPin.ToCentreInteret());
        }

        public async void UpdateStructure()
        {
            CIPin.Nature = NatureList.GetValue(Nature);

            await Main.Service.UpdateStructure(CIPin.ToCentreInteret());
        }
    }
}
