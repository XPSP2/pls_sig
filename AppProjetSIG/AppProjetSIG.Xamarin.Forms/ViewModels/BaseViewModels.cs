﻿using AppProjetSIG.Core.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using Xamarin.Forms;

namespace AppProjetSIG.Xamarin.Forms.ViewModels
{
    /// <summary>
    /// Classe communiquant avec le modèle :(
    /// </summary>
    public class BaseViewModels : INotifyPropertyChanged
    {
        private string title = string.Empty;
        public string Title
        {
            get { return title; }
            set { title = value; OnPropertyChanged(); }
        }

        public BaseViewModels()
        {
            if (Main.Service == null)
            {
                Main.Service = new ServiceImpl();
            }
        }

        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            var changed = PropertyChanged;
            if (changed == null)
                return;

            changed.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}
