﻿using AppProjetSIG.Core.SQL.Table;
using AppProjetSIG.Xamarin.Forms.Views;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace AppProjetSIG.Xamarin.Forms.ViewModels
{
    public class PinDetailViewModels : BaseViewModels
    {
        private CentreInteret centreInteret;
        public CentreInteret CentreInteret {
            get { return centreInteret; }
            set { centreInteret = value; OnPropertyChanged(); }
        }

        private string _nature;
        public string Nature {
            get { return _nature; }
            set { _nature = value; OnPropertyChanged(); }
        }

        private string _distance;
        public string Distance {
            get { return _distance; }
            set { _distance = value; OnPropertyChanged(); }
        }

        public Command LoadCommand { get; set; }

        public PinDetailViewModels(CentreInteret ct = null)
        {
            Title = ct?.Nom;
            CentreInteret = ct;
            
            Nature = StringEnum.GetStringValue(ct.Nature);

            LoadCommand = new Command(async () => await ExecuteAsync());
            LoadCommand.Execute(0);
        }

        public async Task<CentreInteret> FindCentreInteretById(int id)
        {
            return await Main.Service.FindCentreInteretById(id);
        }

        public async Task ExecuteAsync()
        {
            var location = await Geolocation.GetLastKnownLocationAsync();
            var ci = new Location(CentreInteret.Latitude, CentreInteret.Longitude);
            var distance = Location.CalculateDistance(location, ci, DistanceUnits.Kilometers);

            if (distance < 1)
            {
                Distance = Math.Round(distance * 1000, 0) + " m";
            } else
            {
                Distance = Math.Round(distance, 1) + " km";
            }
        }

        public async Task RemoveAsync()
        {
            await Main.Service.RemoveStructure(CentreInteret.Id);
        }
    }
}
