﻿using AppProjetSIG.Core.SQL.Dao;
using AppProjetSIG.Core.SQL.Interface;
using System;

namespace TestNpgsql
{
    class Program
    {
        static void Main(string[] args)
        {
            ITable t = new Table(); t.Connexion();
            t.Insert(new AppProjetSIG.Core.SQL.Table.CentreInteret {
                Id=1,
                Nom = "essai",
                Nature = AppProjetSIG.Core.SQL.Table.Nature.CABINETMEDICAUX,
                Detail = "essai1",
                Latitude = 1,
                Longitude = 1
            });
        }
    }
}
