﻿using AppProjetSIG.Core.SQL.Dao;
using AppProjetSIG.Core.SQL.Interface;
using AppProjetSIG.Core.SQL.Table;
using System;

namespace TestSQLite
{
    class Program
    {
        static void Main(string[] args)
        {
            ITable t = new TableSQLite(); t.Connexion();
            t.Insert(new CentreInteret {
                Id = 1,
                Nom = "essai1",
                Nature = Nature.AUTRE,
                Detail = "Ceci est un essai1",
                Latitude = 0D,
                Longitude = 0D
            });
            t.Insert(new CentreInteret
            {
                Id = 2,
                Nom = "essai2",
                Nature = Nature.AUTRE,
                Detail = "ceci est un essai2",
                Latitude = 1D,
                Longitude = 1D
            });
            foreach(CentreInteret ci in t.FindAll())
            {
                Console.WriteLine(ci.ToString());
            }

            var ci2 = t.Find(1);
            Console.WriteLine(ci2.ToString());
        }
    }
}
